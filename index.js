import{Router,Route,browserHistory,IndexRoute} from 'react-router'
import React from 'react'
import { render } from 'react-dom'
import Head from './components/Head';
import LogPage from './components/LogPage';
import Home from './components/Home';
import BioPage from './components/BioPage';
import ShowPhotos from './components/ShowPhotos';
render((
    <Router>
        <Route path="/" component={Head} >
			<IndexRoute component={LogPage}/>
			<Route 
				path='home/:last_searchKey/:last_natoaSearchKey/:last_lavoriSearchKey/:last_min/:last_max/:token/:last_page/:last_no_data'
				component={Home}
			/>
			<Route
				path='biografia/:nomeId/:myLavori/:myMax/:myMin/:myNatoa/:myNome/:myToken/:myPage/:my_no_data'
				component={BioPage}
			/>
			<Route
				path='show_photo/:nomeId/:myLavori/:myMax/:myMin/:myNatoa/:myNome/:myToken/:myPhoto/:myPage/:myAllPhoto/:myNomeBio/:myNo_anno'
				component={ShowPhotos}
			/>
			
        </Route>
	</Router>

    ), document.getElementById('app'))
