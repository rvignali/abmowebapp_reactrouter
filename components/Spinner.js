import React from 'react';

export default React.createClass({

    getInitialState() {
        document.addEventListener('startWaiting', () => {
            this.setState({spinning:true})
        });
        document.addEventListener('stopWaiting', () => {
            this.setState({spinning:false})
        });
        return {spinning:false};
    },

    render() {
        return (
            <div>
            {this.state.spinning ?
                <div style={{width:"68px", height:"68px", position:"absolute", zIndex:"100000", top: "0", left: "0", bottom:"0", right:"0", margin:"auto"}}>
                    <img src="/assets/images/spinners/slds_spinner_brand.gif" style={{width:"68px"}} alt="Loading..." />
                </div>
            :null}
            </div>
        );
    }

})
