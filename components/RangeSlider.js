import React from 'react';
import $ from 'jquery';


export default React.createClass({
    componentDidMount: function() {
        var $el = $(this.refs.slider.getDOMNode()),
            changeHandler = this.props.onChange;
        $el.noUiSlider({
            start: [ this.props.min_start, this.props.max_start ],
            margin: 10,
			connect: true,
            step: this.props.step,
            range: {
                'min': this.props.min,
                'max': this.props.max
            }
        });
        //$el.noUiSlider_pips({
        //    mode: 'steps',
        //    filter: function(value) {
        //        return value == Math.floor(value);
        //    },
        //    density: 2
        //});
		
        $el.Link('lower').to('-inline-<div class="tooltip"></div>', function ( value ) {
            $(this).html(
                '<span>' + value.substr(0, value.length - 3) + '</span>'
            );
        });
        $el.Link('upper').to('-inline-<div class="tooltip"></div>', function ( value ) {
            $(this).html(
                '<span>' + value.substr(0, value.length - 3) + '</span>'
            );
        });
        $el.on({
            change: function(event){
                changeHandler($el.val());
            }
        });
    },
    render: function () {
        return (
            <div className="slider-wrapper" style={{marginLeft:'4px',marginRight:'4px'}}>
                <span className="slider-label">{this.props.label}</span>
                <div ref="slider"></div>
            </div>
        );
    }
});