import React from 'react';
import config from 'config';

export default React.createClass({
	
	getInitialState: function(){
		return{
			mostraFoto: this.props.params.myPhoto,
			BottoneDestro: "enabled",
			BottoneSinistro: ""
		}
		
	},
	
	returnHome: function() {
		window.location.hash =
			'#biografia'
			+'/'+this.props.params.nomeId
			+'/'+this.props.params.myLavori
			+'/'+this.props.params.myMax
			+'/'+this.props.params.myMin
			+'/'+this.props.params.myNatoa
			+'/'+this.props.params.myNome
			+'/'+this.props.params.myToken
			+"/"+this.props.params.myPage
			+"/"+this.props.params.myNo_anno
		;
    },
	
	avanzaDiUnaFoto: function(){
		var a=this.state.mostraFoto;
		var allPhoto=this.props.params.myAllPhoto.split(",");
		var index=0;
		for(i=0;i<allPhoto.length;i++){
			if(allPhoto[i]==a){
				index=i+1;
				if(index>=allPhoto.length){
					index--;
				}
			}
		}
		this.setState({mostraFoto:allPhoto[index]});
		return null;
	},
	
	indietroDiUnaFoto: function(){
		var a=this.state.mostraFoto;
		var allPhoto=this.props.params.myAllPhoto.split(",");
		var index=0;
		for(i=0;i<allPhoto.length;i++){
			if(allPhoto[i]==a){
				index=i-1;
				if(index<0){
					index=0;
				}
			}
		}
		this.setState({mostraFoto:allPhoto[index]});
		return null;
	},
	
	render: function () {
		if(this.props.params.myAllPhoto.split(',').length>1){
			var val="btn btn-primary btn-block";
		}else{
			var val="btn btn-primary disabled btn-block"
		}
		
		return (
	<div>
	<div className="container">
		<div className="panel panel-primary" style={{height:'auto'}} >
			<div className="panel-heading">
				<div className="row">
					<div className="col-xs-10 col-sm-11 col-md-11 col-lg-11" >
						<p style={{fontSize:'x-large'}}>{this.props.params.myNomeBio}</p>
					</div>
					<div className="col-xs-2 col-sm-1 col-md-1 col-lg-1">
						<button type="button" className={"btn btn-default"} onClick={this.returnHome} >
							<span className="glyphicon glyphicon-remove"></span>
						</button>
					</div>
				</div>
			</div>
			<div className="panel-body">
				
				<div className="row" style={{padding:"5px"}}>
				 
					<div className="col-xs-6 col-md-6" >
						<button style={{height:'49px'}}
								type="button"
								className={val}
								onClick={this.indietroDiUnaFoto}
						>
							<span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						</button>
					</div>
					
					<div className="col-xs-6 col-md-6" >
						<button style={{height:'49px'}} type="button" className={val} onClick={this.avanzaDiUnaFoto} >
							<span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						</button>
					</div>
				
				
				</div>
				
				<div className="row">
					<div className="col-xs-12 col-md-12" >
						<span  className="thumbnail" >
							<img className="img-responsive"
							src={config.pics_path+this.state.mostraFoto}/>
							
						</span>

					</div>
				
				</div>
				
			</div>
			
		</div>
	</div>
</div>		
		);	
	}	

});