import React from 'react';



export default React.createClass({
	
	getInitialState() {
        document.addEventListener('notify', (event) => {
            this.setState({
				showNotification:true,
				message:event.detail.text,
				type:event.detail.type,
				title:event.detail.title
			});
            setTimeout(() => {
                this.setState({showNotification:false});
            }, 1700);
        });
        return {showNotification:false};
    },
	
	closeHandler() {
        this.setState({showNotification:false});
    },
	
	render() {
		
		return (
			<div>
			{this.state.showNotification ? 
				<div 
					style={{width:'400px', height:'120px', position:'absolute', zIndex:'100000', top: '0', left: '0', bottom:'0', right:'0', paddingTop:'1px',margin:'auto'}} 
					className={"alert "+this.state.type} 
					role="alert"
				>
				<a href="#" className="close" onClick={this.closeHandler} data-dismiss="alert" aria-label="close">&times;</a>
				<h3><center><strong>{this.state.title}</strong></center></h3><h4><center>{this.state.message}</center></h4>
				</div>
				:null
			}
			</div>

		);
	}	

});