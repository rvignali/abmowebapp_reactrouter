import React from 'react';
import nominativiService from '../services/dataService';
import PhotoFrame from './PhotoFrame';
export default React.createClass({

	returnHome: function() {
		window.location.hash=
			'#home'
			+'/'+this.props.params.myNome
			+'/'+this.props.params.myNatoa
			+'/'+this.props.params.myLavori
			+'/'+this.props.params.myMin
			+'/'+this.props.params.myMax
			+'/'+this.props.params.myToken
			+'/'+this.props.params.myPage
			+'/'+this.props.params.my_no_data
		;
    },
	
	getInitialState: function() {
        return {
			nominativo: {},
			fonti:[{}],
		};
    },
	
	componentDidMount: function() {
		document.dispatchEvent(new Event('startWaiting'));
        nominativiService.findById(this.props.params.nomeId,this.props.params.myToken)
		.done(function(resultFindById) {
            nominativiService.findFontsById(resultFindById.fondi_individuali,this.props.params.myToken)
				.done(function(resultFindFontsById){
					document.dispatchEvent(new Event('stopWaiting'));
					this.setState({nominativo: resultFindById});
					if (resultFindFontsById.length ==0){resultFindFontsById.push({descrizione:'Nessuna fonte'});} 
					this.setState({fonti: resultFindFontsById});
				}.bind(this))
				.fail(function(xhr,textStatus){
					document.dispatchEvent(new Event('stopWaiting'));
					if(xhr.status==401){
						document.dispatchEvent(new CustomEvent(
							'notify',
							{'detail':{
									'text':'Authentication invalid',
									'type':'alert-info',
									'title':'Denied Access'
								}
							}
						));
					}
					window.location.hash='#/';
				});
		}.bind(this))
		.fail(function(xhr,textStatus){
			document.dispatchEvent(new Event('stopWaiting'));
			if(xhr.status==401){
				document.dispatchEvent(new CustomEvent(
							'notify',
							{'detail':{
									'text':'Authentication invalid',
									'type':'alert-info',
									'title':'Denied Access'
								}
							}
						));
			}
			window.location.hash='#/';
			
		});
	},
	render: function(){
		if(this.state.nominativo.id_foto_size){
			var res=this.state.nominativo.id_foto.split(",");
			var photos=res.map(function (id_photo,index){
				return(
					<PhotoFrame
						key={index}
						id_photograp={id_photo}
						myLavori={this.props.params.myLavori}
						myMax={this.props.params.myMax}
						myMin={this.props.params.myMin}
						myNatoa={this.props.params.myNatoa}
						myNome={this.props.params.myNome}
						myToken={this.props.params.myToken}
						myNomeId={this.props.params.nomeId}
						myPage={this.props.params.myPage}
						allPhoto={res}
						nomeBio={this.state.nominativo.nome}
						no_anno_nascita={this.props.params.my_no_data}
					/>
				);
			}.bind(this));
		} else {
			//var res=[""];
			var photos=null;
		}
		
		
		var natoIn_regione=(this.state.nominativo.regione)?this.state.nominativo.regione:"...";
		var natoIn_nazione=(this.state.nominativo.nazione_di_nascita)?this.state.nominativo.nazione_di_nascita:"...";
		var natoIn_comune=(this.state.nominativo.comune_di_nascita)?this.state.nominativo.comune_di_nascita:"...";
		var natoa=(this.state.nominativo.natoa)?this.state.nominativo.natoa:"...";
		var natoil_giorno=(this.state.nominativo.giorno_e_mese_di_nascita)?this.state.nominativo.giorno_e_mese_di_nascita:"...";
		var natoil_data=(this.state.nominativo.natoil_data)?this.state.nominativo.natoil_data:"...";
		var natoil_anno=(this.state.nominativo.natoil_anno)?this.state.nominativo.natoil_anno:"...";
		var morto_a=(this.state.nominativo.morto_a)?this.state.nominativo.morto_a:"...";
		var mortoIn_regione=(this.state.nominativo.regione_di_morte)?this.state.nominativo.regione_di_morte:"...";
		var comune_di_morte=(this.state.nominativo.comune_di_morte)?this.state.nominativo.comune_di_morte:"...";
		var giorno_e_mese_di_morte=(this.state.nominativo.giorno_e_mese_di_morte)?this.state.nominativo.giorno_e_mese_di_morte:"...";
		var anno_di_morte=(this.state.nominativo.morto_il)?this.state.nominativo.morto_il:"...";
		var nazione_di_morte=(this.state.nominativo.nazione_di_morte)?this.state.nominativo.nazione_di_morte:"...";
		var provincia_di_attività=(this.state.nominativo.provincia)?this.state.nominativo.provincia:"...";
		var comune_di_attività=(this.state.nominativo.comune)?this.state.nominativo.comune:"...";
		var regione_di_attività=(this.state.nominativo.regione)?this.state.nominativo.regione:"...";
		var nome_del_padre=(this.state.nominativo.nome_del_padre)?this.state.nominativo.nome_del_padre:"...";
		var nome_della_madre=(this.state.nominativo.nome_della_madre)?this.state.nominativo.nome_della_madre:"...";
		var studi=(this.state.nominativo.studi)?this.state.nominativo.studi:"...";
		var professioni=(this.state.nominativo.professioni)?this.state.nominativo.professioni:"...";
		var famiglia=(this.state.nominativo.famiglia)?this.state.nominativo.famiglia:"...";
		var politica_1=(this.state.nominativo.politica1)?this.state.nominativo.politica1:"...";
		var politica_2=(this.state.nominativo.politica_2)?this.state.nominativo.politica_2:"...";
		var tipo_biografia=(this.state.nominativo.tipo_biografia)?this.state.nominativo.tipo_biografia:"...";
		var lavori=(this.state.nominativo.lavori)?this.state.nominativo.lavori:"...";
		var libri=(this.state.nominativo.libri)?this.state.nominativo.libri:"...";
		var redatta=(this.state.nominativo.dataredazione)?this.state.nominativo.dataredazione:"...";
		var note=(this.state.nominativo.note)?this.state.nominativo.note:"...";
		var colore_politico=(this.state.nominativo.colore_politico)?this.state.nominativo.colore_politico:"...";
		
		
		
		var testo_fonti=this.state.fonti.map(function (fonte,index){
			return(
				<p 	key={index}
					style={{margin: "10px 0 10px 0",lineHeight:"1.42857143"}}
				>
					-{fonte.materiali} ({fonte.fonti_descr})
				</p>
			)
		}.bind(this));
		return(
			<div>
			
			<div className="container">
			{/* home button */}
				<div className="row">
					<div className="center-block trim">
						<div className="col-md-12 padding" >
							<button type="button" className={"btn btn-default"} onClick={this.returnHome}>
								<span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Home
							</button>
						</div>
					</div>
				</div>
			{/* pannello foto */}	
				<div className="panel panel-primary" style={{height:'auto'}} >
					<div className="panel-heading">
						<p style={{
									fontFamily: 'Arial, Helvetica, sans-serif',
									color: '#fff',
									fontSize:'16px',
									fontStyle:'italic'
									}}>
							Fotografie in archivio di {this.state.nominativo.nome} :
						</p>
					</div>
					<div className="panel-body">
						<div className="row">
							{photos}
						</div>
					</div>
				</div>
			{/* pannello informazioni */}
				<div className="panel panel-primary" style={{height:'auto'}} >
					<div className="panel-heading">
						<p style={{
								fontFamily: 'Arial, Helvetica, sans-serif',
								fontSize:'24px'
									}}>
							{this.state.nominativo.nome}
						</p>
					</div>
                    <div className="panel-body" style={{
									fontFamily: 'Arial, Helvetica, sans-serif',
									fontSize:'18px'
									}}>
						
						<p style={{margin: "10px 0 10px 0",lineHeight:"1.42857143"}}>
							 Nato in <span className="label label-primary">{natoIn_regione}</span>
							 <span className="label label-primary">{natoIn_nazione}</span>
							nel comune di <span className="label label-primary">{natoIn_comune}</span> <span className="label label-primary">{natoa}</span>
							il <span className="label label-primary">{natoil_giorno}</span>
							<span className="label label-primary">{this.state.nominativo.nato_il}</span>
						</p>
						
						<p style={{margin: "10px 0 10px 0",lineHeight:"1.42857143"}}>
							Morto in <span className="label label-primary">{mortoIn_regione}</span>
							<span className="label label-primary">{nazione_di_morte}</span>
							nel comune di <span className="label label-primary">{comune_di_morte}</span>
							<span className="label label-primary">{morto_a}</span>
							il <span className="label label-primary">{giorno_e_mese_di_morte}</span>
							<span className="label label-primary">{anno_di_morte}</span>
						
						</p>
						
						<p style={{margin: "10px 0 10px 0",lineHeight:"1.42857143"}}>
							Comune e provincia di attività<span className="label label-primary">{comune_di_attività}</span>
							<span className="label label-primary">{provincia_di_attività}</span>
							<span className="label label-primary">{regione_di_attività}</span>
							
						</p>
						<p style={{margin: "10px 0 10px 0",lineHeight:"1.42857143"}}>
							Nome del Padre <span className="label label-primary">{nome_del_padre}</span>
							Nome della madre <span className="label label-primary">{nome_della_madre}</span>
						</p>
						
						<p style={{margin: "10px 0 10px 0",lineHeight:"1.42857143"}}>
							Studi <span className="label label-primary">{studi}</span>
						</p>
						<p style={{margin: "10px 0 10px 0",lineHeight:"1.42857143"}}>
							Professioni <span className="label label-primary">{professioni}</span>
						</p>
						
						<p style={{margin: "10px 0 10px 0",lineHeight:"1.42857143"}}>
							Famiglia <span className="label label-primary">{famiglia}</span>
						</p>
						
						<p style={{margin: "10px 0 10px 0",lineHeight:"1.42857143"}}>
							Politica 1 <span className="label label-primary">{politica_1}</span>
							Politica 2<span className="label label-primary">{politica_2}</span>
						</p>
						
						<p style={{margin: "10px 0 10px 0",lineHeight:"1.42857143"}}>
							Tipo biografia <span className="label label-primary">{tipo_biografia}</span>
							Lavori <span className="label label-primary">{lavori}</span> 
							Libri<span className="label label-primary">{libri}</span> 
							Redatta <span className="label label-primary">{redatta}</span> 
						</p>
						
						<p style={{margin: "10px 0 10px 0",lineHeight:"1.42857143"}}>
							Note <span className="label label-primary">{note}</span>
						</p>
						
						<p style={{margin: "10px 0 10px 0",lineHeight:"1.42857143"}}>
							Colore(i) Politico <span className="label label-primary">{colore_politico}</span>
						</p>
						
                    </div>
                </div>
			{/* pannello biografia */}	
				<div className="panel panel-primary" style={{height:'auto'}} >
					<div className="panel-heading">
						<p style={{
									fontFamily: 'Arial, Helvetica, sans-serif',
									fontSize:'18px'
									}}>
							Biografia:
						</p>
					</div>
                    <div className="panel-body">
						<div dangerouslySetInnerHTML={{__html: this.state.nominativo.testo}}/>
                    </div>
                </div>
			{/* pannello fonti*/}	
				<div className="panel panel-primary" style={{height:'auto'}} >
					<div className="panel-heading">
						<p style={{
									fontFamily: 'Arial, Helvetica, sans-serif',
									fontSize:'18px'
									}}>
							Fonti
						</p>
					</div>
                    <div className="panel-body">
						<p style={{margin: "10px 0 10px 0",lineHeight:"1.42857143"}}>
							{testo_fonti}
						</p>
					</div>
				</div>
			{/* home button */}	
				<div className="row">
					<div className="center-block trim">
						<div className="col-md-12 padding" >
							<button type="button" className={"btn btn-default"} onClick={this.returnHome}>
								<span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Home
							</button>
						</div>
					</div>
				</div>
				
			</div>
			</div>
		);
	}


});