import React from 'react';
import {Link} from 'react-router'
import config from 'config'

export default React.createClass({
	
	natoaLinkHandler: function(e) {
        this.props.natoaKeyChange(e.target.innerHTML);
        return false;
    },
	
	lavoriLinkHandler: function(e) {
        this.props.lavoriKeyChange(e.target.innerHTML);
        return false;
    },
	
	render() {
		
		//crea la stringa per il routing del link del nominativo ( var sommatoria )
		var id=this.props.nominativo.id?this.props.nominativo.id:'null';
		var lavori=this.props.my_lavori?this.props.my_lavori:'null';
		var max=this.props.my_max?this.props.my_max:'null';
		var min=this.props.my_min?this.props.my_min:'null';
		var natoa=this.props.my_natoa?this.props.my_natoa:'null';
		var nome=this.props.my_nome?this.props.my_nome:'null';
		var token=this.props.the_token?this.props.the_token:'null';
		var page=this.props.my_page?this.props.my_page:'null';
		var no_data=this.props.my_no_data?this.props.my_no_data:'null';
		var sommatoria='#biografia/'+id+'/'+lavori+'/'+max+'/'+min+'/'+natoa+'/'+nome+'/'+token+'/'+page+'/'+no_data;
		//
		
		
		var links;
		if (this.props.nominativo.lavori) {
            
			var tags = this.props.nominativo.lavori.split(';');
            links = tags.map(function(tag) {
                return <a href="#fakeLink" className="tag" onClick={this.lavoriLinkHandler} key={this.props.nominativo.id + '-' + tag}>{tag}</a>;
            }.bind(this));
        }
		
		
		if(this.props.nominativo.foto){
			var res=this.props.nominativo.foto.split(",");
			var foto="url('"+config.pics_path+res[0] + "')";
		} else {
			var foto="url('./assets/images/generic.png')"
		}
		
		if(parseFloat(this.props.nominativo.nato_il)=='0'){
			var data='?';
		} else {
			var data=parseFloat(this.props.nominativo.nato_il);
		}
		
		var cartella_foto="d:/foto/";
        var divStyle = {"backgroundImage": foto};
		
		return (
			
			<div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 nopadding" key={this.props.nominativo.id}>
                <div className="panel panel-default">
                    <div className="panel-body">
                        <div style={divStyle} className="img-wrapper"/>
						<a href={sommatoria}>
							<h3 className="panel-title">
								{this.props.nominativo.nome}
							</h3>
						</a>
						<p className="level">
							{data}
						</p>
                        <p>
							<img src='./assets/icons/icon-brewery.png' className="icon"/>
							<a href="#fakeLink" onClick={this.natoaLinkHandler}>
								{this.props.nominativo.natoa}
							</a>
						</p>
                        <p><img src='./assets/icons/icon-tags.png' className="icon"/>{links}</p>
						
                    </div>
                </div>
            </div>

		);
	}	

});