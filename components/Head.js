import React from 'react';
import Spinner from './Spinner';
import Toaster from './Toaster';

export default React.createClass({

	render() {
		if(this.props.logOff_btn=='yes'){
			var logOffBtn=
			<a className="navbar-btn" href="#">
						<button type="button" className="btn btn-default navbar-btn navbar-right" style={{marginRight:'5px'}}>Log Off</button>
			</a>
		}else{
			var logOffBtn=null;
		}
		return (
			<div>
				<Spinner/>
				<Toaster/>
				<div className="row">
            		<div className="navbar navbar-default navbar-static-top" role="navigation">
						<div className="col-xs-9 col-md-8">
							<div className="navbar-header" style={{width:'400px'}}>
								<div className="navbar-brand" >
									<img alt="Brand" src="assets\icons\abmo.png" />
								</div>
								<p style={{padding:'11px'}}>Archivio Biografico del Movimento Operaio</p>
							</div>
						</div>
						<div className="col-xs-3 col-md-4">
							<a className="navbar-btn" href="#">
								<button type="button" className="btn btn-default navbar-btn navbar-right" style={{marginRight:'5px'}}>Log Off</button>
							</a>
						</div>
					</div>
				</div>
				{this.props.children}
			</div>

		);
	}	

});