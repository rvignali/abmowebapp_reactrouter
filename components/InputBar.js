import React from 'react';

export default React.createClass({
	
	value : function() {
		var val=this.refs.inputBar.getDOMNode().value;
		this.props.loadValue(val);
    },

	render() {
		
		return (
			<div className="row-margin">
				<div className="input-group input-group-lg">
					<span className="input-group-addon" id="basic-addon1">
						<span className={this.props.titleButton} aria-hidden="true"></span>
					</span>
					<input type="text"  ref="inputBar" className="form-control" placeholder={this.props.plcHolder} aria-describedby="basic-addon1" onChange={this.value}/>
				</div>
			</div>

		);
	}	

});