import React from 'react';
import NamesListItem from './NamesListItem';

export default React.createClass({

	render() {
		
		var items = this.props.nominativi.map(function (nominativo) {
            return (
                <NamesListItem 
					key={nominativo.id}
					nominativo={nominativo}
					natoaKeyChange={this.props.natoaKeyChange}
					lavoriKeyChange={this.props.lavoriKeyChange}
					the_token={this.props.token}
					my_lavori={this.props.lavori}
					my_max={this.props.max}
					my_min={this.props.min}
					my_natoa={this.props.natoa}
					my_nome={this.props.nome}
					my_page={this.props.page}
					my_no_data={this.props.no_data}
				/>
            );
        }.bind(this));
		
		return (
			<div className="container">
                <div className="row">
                    {items}
                </div>
            </div>

		);
	}	

});