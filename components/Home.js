import React from 'react';
import nominativiService from '../services/dataService';
import SearchBar from './SearchBar';
import Nouislider from 'react-nouislider';
import Paginator from './Paginator';
import NamesList from './NamesList';

export default React.createClass({
	
	getInitialState: function() {
		
		var lavori=(this.props.params.last_lavoriSearchKey!='null')?this.props.params.last_lavoriSearchKey:"";
		var key=(this.props.params.last_searchKey!='null')?this.props.params.last_searchKey:"";
		var natoa=(this.props.params.last_natoaSearchKey!='null')?this.props.params.last_natoaSearchKey:"";
		var page=parseInt(this.props.params.last_page);
		if(page){
				var value= page;
			}else{
				value=1;
			}
		if(this.props.params.last_no_data=="true"){
			var str="collapse out";
		}else if(this.props.params.last_no_data=="false"){
			var str="collapse in";
		}
        
		
		
		return {
			no_data:this.props.params.last_no_data,
			rangeSlider: str,
			my_token: this.props.params.token,
            searchKey: key,
            min: parseInt(this.props.params.last_min),
            max: parseInt(this.props.params.last_max),
			lavoriSearchKey: lavori,
			natoaSearchKey: natoa,
			pageSize:12,
            nominativi: [],
            total: 0,
            page: value
        }
    },
	
	searchKeyParser: function(value){
		var stringOfValues=value;
		var values=value.split(',');
		var valuesClean=[];
		values.forEach( function(value){
			if(value.trim == ""){valuesClean.push("")}
			else if(value.trim == "*"){valuesClean.push("")}
			else {valuesClean.push(value)};
		});
		console.log(valuesClean);
		return stringOfValues;	
	},
	
	lavoriSearchKeyHandler: function(lavori){
		var lavoriUpperCase=lavori.toUpperCase();
		this.setState({lavoriSearchKey: lavoriUpperCase, page:1},this.findNominativi);
	},
	
	natoaSearchKeyHandler: function(natoa) {
		var natoaUpperCase=natoa.toUpperCase();
		this.setState({natoaSearchKey: natoaUpperCase, page: 1},this.findNominativi);
	},
	
	searchKeyChangeHandler: function(searchKey) {
        this.setState({searchKey: searchKey, page: 1}, this.findNominativi);
    },
	
	rangeChangeHandler: function(values) {
		function remDec(decValue){
			var valStr=String(decValue);
			var res=valStr.substring(0,valStr.indexOf('.'));
			return res;
		}
		
        this.setState({min: remDec(values[0]), max: remDec(values[1]), page: 1}, this.findNominativi);
    },
	
	componentDidMount: function() {
		
        this.findNominativi();
    },
	
	findNominativi: function() {
		if(this.state.no_data=="true"){
			document.dispatchEvent(new Event('startWaiting'));
			nominativiService.findAllSenzaData(
				{
					my_token: this.state.my_token,
					search: this.state.searchKey,
					natoa: this.state.natoaSearchKey,
					lavori: this.state.lavoriSearchKey,
					page: this.state.page,
					pageSize: this.state.pageSize
				}
			).done (function(data, textStatus, xhr) {
					document.dispatchEvent(new Event('stopWaiting'));
					this.setState({
						nominativi: data,
						page: this.state.page,
						total: xhr.getResponseHeader('X-Total-Count')
				
					});
			
				}.bind(this)
		
			).fail(function(xhr,textStatus){
				document.dispatchEvent(new Event('stopWaiting'));
				if(xhr.status==401){
					window.location.assign('');
					alert("Autorizzazione Scaduta! Reinserire nome utente e password");
				}
			});
		} else if(this.state.no_data=="false"){
			document.dispatchEvent(new Event('startWaiting'));
			nominativiService.findAll(
				{
					my_token: this.state.my_token,
					search: this.state.searchKey,
					natoa: this.state.natoaSearchKey,
					lavori: this.state.lavoriSearchKey,
					min: this.state.min,
					max: this.state.max,
					page: this.state.page,
					pageSize: this.state.pageSize
				}
			).done (function(data, textStatus, xhr) {
					document.dispatchEvent(new Event('stopWaiting'));
					this.setState({
						nominativi: data,
						page: this.state.page,
						total: xhr.getResponseHeader('X-Total-Count')
				
					});
			
				}.bind(this)
		
			).fail(function(xhr,textStatus){
				document.dispatchEvent(new Event('stopWaiting'));
				if(xhr.status==401){
					window.location.assign('');
					alert("Autorizzazione Scaduta! Reinserire nome utente e password");
				}
			});
		}
    },
	
	nextPage: function() {
        var p = this.state.page + 1;
        this.setState({page: p}, this.findNominativi);
    },
	
    prevPage: function() {
        var p = this.state.page - 1;
        this.setState({page: p}, this.findNominativi);
    },
	
	changeMode: function() {
		
       this.setState(
		{
			no_data:(this.state.no_data=="false")?"true":"false",
			rangeSlider:(this.state.no_data=="false")?"collapse out":"collapse in",
			searchKey: "",
            min: 1760,
            max: 1940,
			lavoriSearchKey: "",
			natoaSearchKey: "",
			pageSize:12,
            nominativi: [],
            total: 0,
            page: 1
		}
	   
	   , this.findNominativi);
    },
	
	
	render: function() {
		
		return (
			<div>
			
				<div className="row" style={{margin:'auto'}}>
					<div className="center-block trim">
						<div className="col-sm-8 col-xs-6">
							<SearchBar 
								searchKey={this.state.searchKey} 
								placehold="Nome:" 
								searchKeyChange={this.searchKeyChangeHandler}
							/>
						</div>
						<div className="col-sm-2 col-xs-3">
							<SearchBar 
								searchKey={this.state.natoaSearchKey} 
								placehold="Nato a:" 
								searchKeyChange={this.natoaSearchKeyHandler}
							/>
						</div>
						<div className="col-sm-2 col-xs-3">
							<SearchBar 
								searchKey={this.state.lavoriSearchKey} 
								placehold={"Lavori:"} 
								searchKeyChange={this.lavoriSearchKeyHandler}
							/>
						</div>
					</div>
				</div>
				
				<div className="row" style={{margin:'auto'}}>
					<div className="center-block slider"
						style={{borderStyle:'solid',
								borderWidth:'1px',
								borderColor:'#c5c9cc',
								borderRadius:'4px',
								marginTop:'3px'}}
					>
					
						<button type="button"
								className="btn btn-primary btn-xs"
								style={{marginTop:'3px',
										marginBottom:'3px',
										backgroundColor:'#336699'
										}}
								data-toggle="collapse"
								data-target="#slider"
								onClick={this.changeMode}
						>
						attiva/disattiva data di nascita
						</button>
						<div id="slider" className={this.state.rangeSlider}>
							<div className="slider-wrapper" style={{marginLeft:'4px',marginRight:'4px'}}>
								<span className="slider-label">anno di nascita</span>
								<Nouislider
									range={{min:1750,max:1950}}
									start={[this.state.min,this.state.max]}
									connect={true,false,true}
									step={5}
									tooltips = {[
										{to: function(value) {return Math.round(parseInt(value));}},
										{to: function(value) {return Math.round(parseInt(value));}}
										]}
									onChange={this.rangeChangeHandler}
								/>
							</div>
						</div>
						
                    </div>
					
                </div>
				
				<Paginator 
					page={this.state.page}
					pageSize={this.state.pageSize}
					total={this.state.total}
					previous={this.prevPage}
					next={this.nextPage}
				/>
				
				<NamesList 
					nominativi={this.state.nominativi}
					total={this.state.total}
					natoaKeyChange={this.natoaSearchKeyHandler}
					lavoriKeyChange={this.lavoriSearchKeyHandler}
					lavori={this.state.lavoriSearchKey}
					natoa={this.state.natoaSearchKey}
					nome={this.state.searchKey}
					min={this.state.min}
					max={this.state.max}
					token={this.state.my_token}
					page={this.state.page}
					no_data={this.state.no_data}
				/>
				
			</div>
			

		);
	}	

});