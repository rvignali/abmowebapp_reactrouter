import React from 'react';
import config from 'config';
import nominativiService from '../services/dataService';
export default React.createClass({
	
	getInitialState: function() {
        return {
			photograph: "",
			
		};
    },
	
	photoPromise: function (id_photo,token){
		return new Promise(function(resolve,reject){
			nominativiService.findPhotoById(
				{id: id_photo, token: this.props.myToken}
			).done(data=>{resolve(data)})
		}.bind(this))
	},
	
	componentDidMount: function() {
	
		this.photoPromise(this.props.id_photograp,this.props.myToken).then(data=>{
			this.setState({photograph: data.file})
		})
	},
	
	render() {
		
		return (
			
			<div className="col-xs-4 col-sm-3 col-md-2 col-lg-2">
				<a 
					href={
							"#show_photo"+
							"/"+this.props.myNomeId+
							"/"+this.props.myLavori+
							"/"+this.props.myMax+
							"/"+this.props.myMin+
							"/"+this.props.myNatoa+
							"/"+this.props.myNome+
							"/"+this.props.myToken+
							"/"+this.state.photograph+
							"/"+this.props.myPage+
							"/"+this.props.allPhoto+
							"/"+this.props.nomeBio+
							"/"+this.props.no_anno_nascita
						}
					className="thumbnail"
				>
					<img 
						src={config.pics_path+this.state.photograph}
						alt={config.pics_path+"generic.png"}
						style={{height:'150px',width:'150px'}}
					/>
					
				</a>
			</div>

		);
	}	

});