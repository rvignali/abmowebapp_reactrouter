import React from 'react';

export default React.createClass({

	render() {
		
		var pages = Math.ceil(this.props.total/this.props.pageSize);
		
		return (
			
			<div className="container">
                <div className="row padding" style={{height: "40px"}}>
                    <div className="col-xs-4 nopadding">
                        <button type="button" className={"btn btn-default" + (this.props.page <= 1 ? " hidden" : "")} onClick={this.props.previous}>
                            <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Previous
                        </button>
                    </div>
                    <div className="col-xs-4 text-center">
                        <div className="legend">{this.props.total} nominativi • pagina {this.props.page}/{pages}</div>
                    </div>
                    <div className="col-xs-4 nopadding">
                        <button type="button" className={"btn btn-default pull-right" + (this.props.page >= pages ? " hidden" : "")} onClick={this.props.next}>
                        Next <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        </button>
                    </div>
                </div>
            </div>

		);
	}	

});