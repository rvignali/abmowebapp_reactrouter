import React from 'react';


export default React.createClass({
	
    searchKeyChangeHandler: function() {
        var searchKey = this.refs.searchKey.getDOMNode().value;
        this.props.searchKeyChange(searchKey);
    },
	
    clearText: function() {
        this.refs.searchKey.getDOMNode().value = "";
        this.props.searchKeyChange("");
    },
	
    render: function () {
        return (
            <div className="search-wrapper">
                <input type="search" ref="searchKey" className="form-control"
                    placeholder={this.props.placehold}
                    value={this.props.searchKey}
                    onChange={this.searchKeyChangeHandler}/>
                <button className="btn btn-link" ><span className="glyphicon glyphicon-remove" aria-hidden="true" onClick={this.clearText}></span></button>
            </div>
        );
    }
});