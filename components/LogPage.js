import React from 'react';
import nominativiService from '../services/dataService';
import InputBar from './InputBar';

export default React.createClass({
	
	getInitialState: function() {
        return {
            user: "",
            pswd: "",
			
        }
    },
	
	setUser: function(valore){
		this.setState({user: valore});
	},
	
	setPassword: function(valore){
		this.setState({pswd: valore});
	},
	
	getToken: function() {
		document.dispatchEvent(new Event('startWaiting'));
		this.refs.pulsante.className="btn btn-primary disabled";
		nominativiService.getToken({utente: this.state.user, pass: this.state.pswd})
		.done(function(data){
			document.dispatchEvent(new Event('stopWaiting'));
			window.location.hash =
			"#home/"
			+"null/"
			+"null/null/1760/1940/"
			+data.token
			+"/null/false";
			
		}.bind(this))
		.fail(function(){
			document.dispatchEvent(new Event('stopWaiting'));
			document.dispatchEvent(new CustomEvent(
					'notify',
					{'detail':{
							'text':'Wrong Username or Password',
							'type':'alert-info',
							'title':'Denied Access'
						}
					}
				)
			);
			
		});
		this.refs.pulsante.className="btn btn-primary enabled";
		
	},

	render() {
		
		return (
			<div>
			<div className="center-block trim">
				<InputBar loadValue={this.setUser} titleButton="glyphicon glyphicon-user" plcHolder="user name"/>
				<InputBar loadValue={this.setPassword} titleButton="glyphicon glyphicon-ok" plcHolder="password"/> 
				<button ref="pulsante" className="btn btn-primary enabled" type="button" onClick={this.getToken}>
					Log In <span className="glyphicon glyphicon-log-in" aria-hidden="true"></span>
				</button>
				</div>
			</div>

		);
	}	

});