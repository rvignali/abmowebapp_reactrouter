var webpack = require('webpack')
var path = require ('path')

module.exports = {
    entry: './index.js',

    output: {
        path: 'public',
        filename: 'bundle.js',
        publicPath: ''
    },
	
	resolve: {
		alias: {
			config: path.join(__dirname, 'config', process.env.NODE_ENV)
		}
	},	
		
    plugins: process.env.NODE_ENV === 'production' ? [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin()
    ] : [],

    module: {
        loaders: [
            { 
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader?presets[]=es2015&presets[]=react' 
            },

            { 
                test: /\.css$/, 
                loader: "style-loader!css-loader" 
            },
			
			{ 
				test: /\.json$/,
				loader: 'json' 
			},

        ]
    },
    devtool: 'source-map'
}

