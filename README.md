# README #

Questo README documenta le istruzioni per installare e configurare questa App

* Configurazione

Installare le dipendencies con:
       npm i

Impostare la variabile di ambiente NODE_ENV:
      NODE_ENV='production' oppure NODE_ENV='development'

Editare le configurazioni degli ambienti di sviluppo e di produzione presenti nella cartella 'config':
      development.js
      production.js

Avviare con:
      npm start