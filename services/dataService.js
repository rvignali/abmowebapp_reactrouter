import $ from 'jquery';
import config from 'config';


export default (function () {
	var baseURL = config.baseURLApi;
	var baseURLAuth = config.baseURLAuth;
    
    // The public API
    return {
        findById: function(id,token) {
            return $.ajax({
				"url":
					baseURL
					//+'/biofulls/'+id
					+'/namefulls/'+id
					+'?access_token='+token
					,
				"method": "GET"
				
	    		});
        },
		
		findPhotoById: function(obj) {
            return $.ajax({
				"url":
					baseURL
					//+'/biofulls/'+id
					//+'/namefulls/'+id
					+'/pictures/'+obj.id
					+'?access_token='+obj.token
					,
				"method": "GET"
				
	    		});
        },
		
		findFontsById: function(id,token) {
			return $.ajax({
				"url":
					baseURL
					+'/fondiindividualis?'
					+'_start=0&_end=100'
					+'&access_token='+token
					+'&idfondiindividuali_in='+id
					,
				"method": "GET"
			});
		},
		
		findAll: function(values) {
            return $.ajax({
				"url":
					baseURL
					+'/names?_sort=nome'
					+'&nato_il_gte='+values['min']
					+'&nato_il_lte='+values['max']
					+'&_start='+(values["pageSize"]*(values['page']-1))
					+'&_end='+(values["pageSize"]*(values['page']))
					+ (!(values['search']==='')?'&nomelower_like='+values['search'].toLowerCase():'')
					+ (!(values['natoa']==='')?'&natoa_like='+values['natoa']:'')
					+ (!(values['lavori']==='')?'&lavori_like='+values['lavori']:'')
					+'&access_token='+values['my_token']
					,
				"method": "GET"
				
	    		});
        },
		
		findAllSenzaData: function(values) {
            return $.ajax({
				"url":
					baseURL
					+'/names?_sort=nome' //usare con  server
					+'&_start='+(values["pageSize"]*(values['page']-1))
					+'&_end='+(values["pageSize"]*(values['page']))
					+ (!(values['search']==='')?'&nomelower_like='+values['search'].toLowerCase():'')
					+ (!(values['natoa']==='')?'&natoa_like='+values['natoa']:'')
					+ (!(values['lavori']==='')?'&lavori_like='+values['lavori']:'')
					+'&access_token='+values['my_token']
					,
				"method": "GET"
				
	    		});
        },
		
		getToken: function(auth) {
			return $.ajax({
				"url": baseURLAuth + '/local/',
				"method": "POST",
				"headers": {
					
					"content-type": "application/x-www-form-urlencoded"
				},
				"data": {
					"email": auth['utente'],
					"password": auth['pass']
				}
			});
		}
			
	};
}());
